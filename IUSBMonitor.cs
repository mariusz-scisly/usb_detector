﻿using System.Management;

namespace USBMonitor
{
    interface IUSBMonitor
    {
        void StartMonitoring();
        void StopMonitoring();
    }
    class USBMonitor : IUSBMonitor
    {
        private ManagementEventWatcher watcher;
        private WqlEventQuery query;
        private object lockObject = new object();
        private bool isDebouncing = false;

        public void StartMonitoring()
        {
            InitializeWatcher();

            watcher.EventArrived += (sender, eventArgs) =>
            {
                var eventType = eventArgs.NewEvent.ClassPath.ClassName;

                lock (lockObject)
                {
                    if (!isDebouncing)
                    {
                        isDebouncing = true;

                        Task.Delay(TimeSpan.FromSeconds(1)).ContinueWith(_ =>
                        {
                            isDebouncing = false;

                            if (eventType == "__InstanceCreationEvent")
                            {
                                Console.WriteLine("Podłączono urządzenie USB");
                            }
                            else if (eventType == "__InstanceDeletionEvent")
                            {
                                Console.WriteLine("Odłączono urządzenie USB");
                            }
                        });
                    }
                }
            };

            watcher.Start();
        }

        public void StopMonitoring()
        {
            if (watcher != null)
            {
                watcher.Stop();
                watcher.Dispose();
            }
        }

        private void InitializeWatcher()
        {
            watcher = new ManagementEventWatcher();
            query = new WqlEventQuery(
                "SELECT * FROM __InstanceOperationEvent WITHIN 3 WHERE TargetInstance ISA 'Win32_PnPEntity'");
            watcher.Query = query;
        }
    }
}
