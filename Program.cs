﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Threading;
using System.Threading.Tasks;

namespace USBMonitor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Program monitorujący USB");

            IUSBMonitor usbMonitor = new USBMonitor();

            usbMonitor.StartMonitoring();

            Console.WriteLine("Naciśnij Enter, aby zakończyć program...");
            Console.ReadLine();

            usbMonitor.StopMonitoring();
        }
    }

   
}
